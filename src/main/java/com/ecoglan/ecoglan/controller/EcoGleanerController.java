package com.ecoglan.ecoglan.controller;

import com.ecoglan.ecoglan.models.EcoGleaner;
import com.ecoglan.ecoglan.repositories.EcoGleanerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EcoGleanerController {
    @Autowired
    private EcoGleanerRepository ecoGleanersRepository;

    @CrossOrigin
    @RequestMapping(value = "EcoGleaners/list", method = RequestMethod.GET)
    public List<EcoGleaner> ecoGleanersList(){
        return ecoGleanersRepository.findAll();
    }

}
